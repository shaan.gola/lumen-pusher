<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TestBroadcast extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Some message.
     *
     * @var string
     */
    public $message;

    /**
     * Create a new event instance.
     *
     * @param  string $message
     * @return void
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('my-channel');
        return ['my-channel'];
    }
    public function broadcastAs() {

        return 'my-event';
        
        }
}
