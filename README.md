# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).


## Pusher Integration.

Step 1:

Create ##config folder and copy broadcasting file from the laravel normal installation.

Step 2:

Edit .env file and enter the following content for pusher configs.


BROADCAST_DRIVER=pusher


PUSHER_APP_ID=XXXXX
PUSHER_APP_KEY=XXXXXXXXX
PUSHER_APP_SECRET=XXXXXXXXXX
PUSHER_APP_CLUSTER=ap2

Step 3 :

Edit bootstrap/app.php file and add following code

`
$app->configure('broadcasting');
$app->register(\Illuminate\Broadcasting\BroadcastServiceProvider::class);
`

Step 4 :

Now add app/Events/TestBroadcast.php (take a look in the repository)

Step 5 : 

Add the following code in routes/web.php for test broadcasting to the channel and event.

`$router->get('test', function () {
    event(new \App\Events\TestBroadcast('Hey now!'));
});`



## Contributing

Thank you for considering contributing to Lumen! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
